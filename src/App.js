import React, { Component } from 'react';
import './App.css';
import Login from './Views/Login';
import Dash from './Views/Dash';


export default class App extends Component {

  constructor(props){
    super(props);

    //state
    this.state= {
      isLoggedIn: false,
      user:{
        id: 1,
        username: 'Dexter',
        password: 'dexterslab'
      }
    };

    //binders
    this.logIn = this.logIn.bind(this);
    this.logout = this.logout.bind(this);
    this.signUp = this.signUp.bind(this);
  }

  signUp() {
    
  }

  logIn(){
    const { isLoggedIn } = this.state;

    this.setState({
      isLoggedIn: true
    })
  
  }

  logout(){
    const { isLoggedIn } = this.state;

    if(isLoggedIn){
      this.setState({
        isLoggedIn: false
      })
    }
    
  }

  render() {

    const { isLoggedIn, user } = this.state;

    if(isLoggedIn){
      return (
        <>
        
          <Dash 
            user = {user}
            logout = {this.logout}
          />
        
        </>
      )
    }

    return (
      <>
  
          <Login 
            logIn = {this.logIn}
            user = {user}
          /> 
      
      </>
    );
  };
}


