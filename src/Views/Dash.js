import React, { Component } from 'react';

import '../Styles/Dash.css'

export default class Dash extends Component {
  constructor(props){
    super(props);

    this.state = {

    }

    this.renderDash = this.renderDash.bind(this);

  }

  renderDash() {
    const { user } = this.props;

    return (
      <div>
        Hello, { user.username }
        <button onClick={ () => {
          this.props.logout();
        } }>Log Out</button>
      </div>
    );
    
  }

  render() {

    return (
        <div>
            {this.renderDash()}
        </div>
    );
  }
}
