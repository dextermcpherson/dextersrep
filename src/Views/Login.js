import React, { Component } from 'react';
import '../Styles/Login.css';
import { FaChevronRight } from 'react-icons/fa';

export default class Login extends Component {

  constructor(props){
    super(props);

    this.state = {
      view: 'signIn',
      username: '',
      password: '',
      verifyPassword: '',
    }

    //renders
    this.renderLoginScreen = this.renderLoginScreen.bind(this);

    this.switchSignIn = this.switchSignIn.bind(this);
    this.switchSignUp = this.switchSignUp.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  } 

  switchSignIn(){
    const { view } = this.state;

    this.setState({
      view: "signIn"
    })
  }

  switchSignUp(){
    const { view } = this.state;

    this.setState({
      view: "signUp"
    })
  }

  handleChange(e){
    const { username, password } = this.state;

    this.setState({
      [e.target.name]: e.target.value
    });

  }

  handleLogin(){
    const { username, password } = this.state;
    const { user } = this.props;

    if(username == user.username && password == user.password ){
      this.props.logIn()
    }else{
      alert("Login failed!")
    }
  }

  renderLoginScreen(){
    const { view, username, password } = this.state;

    switch(view){
      case "signIn":

        return (
          <div className='login-screen'>
              <div className='login-background' style={{ backgroundImage: `url(${ process.env.PUBLIC_URL + '/assets/loginBackground_V3.png' })` }} />
              <div className='login-form'>
                  <div className='logo'>
                      Dexter's
                      <span>Laboratory</span>
                      <div className='divider-bar'></div>
                  </div>
                  <div className='sign-up-in'>
                    <div className='sign-in' onClick={() => {
                      this.switchSignIn();
                    }}>
                      Sign In
                    </div>
                    <div className='sign-up' onClick={() => {
                      this.switchSignUp();
                    }}>
                      Sign Up
                    </div>
                  </div>
                  <div className='input-container'>
                    <input 
                      type="text" 
                      className='input-username' 
                      name='username' 
                      placeholder='username'
                      value={username}
                      onChange={this.handleChange}
                    />
                    <input 
                      type="password" 
                      className='input-password' 
                      name='password' 
                      placeholder='password'
                      value={password}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className='login-button-container'>
                    <button 
                      type='submit' 
                      className='login-button'
                      onClick={() => {
                        this.handleLogin();
                      }}
                    ><FaChevronRight/></button>
                  </div>
              </div>
          </div>
        )
      break;

      case "signUp":
        return(
          <div className='login-screen'>
              <div className='login-background' style={{ backgroundImage: `url(${ process.env.PUBLIC_URL + '/assets/loginBackground_V3.png' })` }} />
              <div className='login-form'>
                  <div className='logo'>
                      Dexter's
                      <span>Laboratory</span>
                      <div className='divider-bar'></div>
                  </div>
                  <div className='sign-up-in'>
                    <div className='sign-in-inactive' onClick={() => {
                      this.switchSignIn();
                    }}>
                      Sign In
                    </div>
                    <div className={`sign-up ${ (view == 'signUp') && "selected" }`} onClick={() => {
                      this.switchSignUp();
                    }}>
                      Sign Up
                    </div>
                  </div>
                  <div className='input-container'>
                    <input type="text" className='input-username' placeholder='username'/>
                    <input type="password" className='input-password' placeholder='password'/>
                    <input type="password" className='input-password' placeholder='verify password'/>
                  </div>
                  <div className='login-button-container'>
                    <button type='submit' className='login-button'><FaChevronRight/></button>
                  </div>
              </div>
          </div>
        )
    };

  }


  render() {
    return (
        <div className='login-screen'>
          {this.renderLoginScreen()}
        </div>
    );
  }
}
